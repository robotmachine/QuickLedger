# Changelog

## v1.0.2 (2019-Mar-21)
* Fixed bug where category from .qlrc file would override `-c` or `-e` set on command line. Issue #4.
* Changed priorities so that setting both `-c` and `-e` on command line will prefer `-c`. 