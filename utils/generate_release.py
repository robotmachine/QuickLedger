import requests, json

# Set Project Key & QL Version
project_key = "5020786"
ql_ver = '1.0.3'

# Needs creds.json file with the syntax:
# {
#   "token": "{{auth_token}}"
# }
creds = json.load(open('creds.json'))

url = f'https://gitlab.com/api/v4/projects/{project_key}/releases'

data = {}
data['name'] = f'QuickLedger v{ql_ver}'
data['tag_name'] = f'v{ql_ver}'
data['description'] = "* Fix Thing One\n* Fix Thing Two"
payload = json.dumps(data)

headers = {
    'Private-Token': creds['token'],
    'Content-Type': "application/json"
    }

response = requests.request("POST", url, data=payload, headers=headers)

if int(str(response.status_code)[:1]) != 2:
    print(f'Failed to create release.\n{response.status_code}: {response.reason}')
else:
    print(f'Release created.')
